//
//  ViewController.swift
//  ExchangeRateAPI
//
//  Created by Bisma Satria Wasesasegara on 26/09/18.
//  Copyright © 2018 Bisma Satria Wasesasegara. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rateCoversionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        let cell = currencyListTable.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        var rateList: [String] = []
        rateList.append(contentsOf: rateCoversionList.keys)
        cell.currencyLabel.text = rateList[indexPath.row]
        cell.rateLabel.text = formatter.string(from: rateCoversionList[rateList[indexPath.row]] as! NSNumber)
        return cell
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return rateCoversionList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currencyTitle[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pick = currencyTitle[row]
        currencyTextField.text = pick
        print("Base currency will change to \(pick) after next refresh")
        updateData()
    }

    @IBOutlet weak var currencyTextField: UITextField!
    @IBOutlet weak var currencyListTable: UITableView!
    
//    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var pickerPositionUpperY: CGFloat! = nil
    var pickerPositionLowerY: CGFloat! = nil
    var rateCoversionList: [String : Any] = [:]
    var currencyTitle: [String] = []
    
    let currencyPicker: UIPickerView = UIPickerView()
    var pick: String = "EUR"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currencyPicker.dataSource = self
        currencyPicker.delegate = self
        
        currencyListTable.dataSource = self
        currencyListTable.delegate = self
        
        updateData()
        currencyTextFieldTouched()
        print(currencyTitle)
    }
    
    func updateData() {
//        DispatchQueue.global().async {
//            while true {
                let session = URLSession(configuration: .default)
                let url = URL(string: "https://api.exchangeratesapi.io/latest?base=\(self.pick)")
                //        var urlRequest = URLRequest(url: url!)
                let dataTask = session.dataTask(with: url!) { (data, response, error) in
                    if let unwrappedError = error {
                        print("Got error \(unwrappedError.localizedDescription)")
                    } else if let unwrappedData = data {
//                        print(unwrappedData)
                        do {
                            let json = try JSONSerialization.jsonObject(with: unwrappedData, options: [])
                            //                    print(json)
                            if let dictionary = json as? [String : Any] {
                                self.currencyTitle.removeAll()
                                self.rateCoversionList[dictionary["base"] as! String] = 1
                                self.currencyTitle.append(dictionary["base"] as! String)
                                for (k, v) in dictionary["rates"] as! [String : Any] {
                                    self.rateCoversionList[k] = v
                                    self.currencyTitle.append(k)
                                }
                                DispatchQueue.main.async {
                                    self.currencyPicker.reloadAllComponents()
                                    self.currencyListTable.reloadData()
                                }
                            }
                        } catch {
                            print("Cannot fetch data.")
                        }
                    }
                }
                dataTask.resume()
//                sleep(3)
//                print("Repeat refresh 3s.")
//            }
//        }
    }
}

