//
//  TableViewCell.swift
//  ExchangeRateAPI
//
//  Created by Bisma Satria Wasesasegara on 27/09/18.
//  Copyright © 2018 Bisma Satria Wasesasegara. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
