//
//  Initiation.swift
//  ExchangeRateAPI
//
//  Created by Bisma Satria Wasesasegara on 26/09/18.
//  Copyright © 2018 Bisma Satria Wasesasegara. All rights reserved.
//

import UIKit

extension ViewController {
    
    func currencyTextFieldTouched() {
        currencyTextField.inputView = currencyPicker
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
        
        let bar = UIToolbar()
        let reset = UIBarButtonItem(title: "Dismiss", style: .plain, target: self, action: #selector(dismissKeyboard))
        bar.items = [reset]
        bar.sizeToFit()
        currencyTextField.inputAccessoryView = bar
    }

    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}
